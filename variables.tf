variable "aws_access_key" { }

variable "aws_secret_key" { }

variable "key_pair" { }

variable "mysql_pass" { }

variable "mysql_user" { }

variable "mysql_dbname" { }

variable "kms_key_id" { }
